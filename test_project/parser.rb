require 'csv'
require 'open-uri'
require 'optparse'
require 'nokogiri'
require 'pry'
require 'json'

options = {}

optparse = OptionParser.new do |opts|
  opts.on('-url', '--url URL_ADRESS') do |url|
    options[:url] = url
  end

  opts.on('-output', '--output NAME_FILE') do |file|
    options[:file] = file
  end
end

optparse.parse!

raise OptionParser::MissingArgument if options[:url].nil?
raise OptionParser::MissingArgument if options[:file].nil?

class Parser

  def initialize(start_url, output_file)
    @start_url = start_url
    @domain = URI.parse(@start_url).scheme + "://" + URI.parse(@start_url).host.downcase
    @output_file = output_file
  end

  def open_url(url)
    html = open(url)
    puts "Open URL: #{url}" 
    Nokogiri::HTML(html)
  end

  def cleanup(str)
    str.gsub(":fle", ":\"\"").gsub(/(\\u\d{3})/, "")
  end

  def create_document()

    processed_products = []

    category_list = open_url(@start_url)

    CSV.open("myfile.csv", "w") do |csv|

      category_list.xpath("//div[contains(@class, 'block_content')]//ul[contains(@class, 'list-block')]/li").each do |all_category|
        category_pages = []
        
        link_category = all_category.xpath(".//a/@href").text

        first_category_page = open_url(link_category)
        category_pages << first_category_page
        first_category_page.xpath("//ul[contains(@class, 'pagination')]/li[not(@class)]//a//@href").each do |next_category_url|
          category_pages << open_url(@domain + next_category_url.text) 
        end

        category_pages.each do |category_page|
          category_page.xpath("//div[contains(@class, 'productlist')]//a[contains(@class, 'product_img_link')]/@href").each do |product_url|

            current_link_item = open_url(product_url.text)

            regex = /combinations = ([^;]+)/
            matches = regex.match(current_link_item.to_s)
            
            if matches 
              string_combinations = matches[0].to_s.delete('combinations = ') 
              combinations = JSON.parse(cleanup(string_combinations))
            else
              combinations = {}
              price_without_weight = current_link_item.xpath("//span[@id='our_price_display']").text
              combinations = {"1" => {"price"=>price_without_weight,"weigth"=>"none","no_variation"=>true}}
            end

            regex_image_id = /idDefaultImage = ([^;]+)/
            matches_image_id = regex_image_id.match(current_link_item.to_s)
            image_id = matches_image_id[0].to_s.delete('idDefaultImage = ')
                
            name_link_item = current_link_item.xpath(".//h1[contains(@class, 'nombre_producto')]").text.strip

            combinations.each do |key, value|
              
              if value['no_variation']
                price = value['price']
                weigth = value['weigth']
              else
                attr_value = value['rue'][0]

                radio_button_for_attr = current_link_item.xpath(".//input[@type='radio' and @value=#{attr_value}]")
                
                price = radio_button_for_attr.xpath("../li//span[@class='attribute_price']").text.gsub(/[\s]/, '') 
                weigth = radio_button_for_attr.xpath("../li//span[@class='attribute_name']").text.strip
              end                                                                   

              full_name = "#{name_link_item} - #{weigth}"
              image_url = current_link_item.xpath("//img[@class='img-responsive' and @id='thumb_#{image_id}']//@src").text

              key_current_item = name_link_item + " - " + weigth + " - " + price + " - " + image_url

              if processed_products.include?(key_current_item)
                puts "Duplicate #{key_current_item}"
              else 
                csv << [full_name, price, image_url]
                processed_products.push(key_current_item)
              end
            end
          end
        end
      end
    end                                                                                                                                                   
  end

  def run
    create_document 
  end

end

Parser.new(options[:url], options[:file]).run