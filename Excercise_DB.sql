drop database Excercise_BD;
create database Excercise_BD;
use Excercise_BD;

create table Customers(
id_customer int primary key,
name_customer varchar(30),
city_customer varchar(30),
phone_customer varchar(30)
);

create table Orders(
id_order int primary key,
customer_id int,
payment int,
foreign key (customer_id) references Customers(id_customer) on delete cascade on update cascade
);

insert into Customers values(1,'Евгений','Минск','+385123466');
insert into Customers values(2,'Александр','Минск','+21312661');
insert into Customers values(3,'Максим','Пинск','+123123123');
insert into Customers values(4,'Александр','Москва','+801231233');
insert into Customers values(5,'Ульяна','Мосвка','+1231215123');
insert into Customers values(6,'Олег','Минск','+123123123');
insert into Customers values(7,'Никита','Ульяновск','+1231251626');
insert into Customers values(8,'Вася','Новгород','+645645645');
insert into Customers values(9,'Петя','Могилев','+4572342562');
insert into Customers values(10,'Настя','Омск','+234236277');
insert into Customers values(11,'Арарат','Махачкала','+464243242');
insert into Customers values(12,'Олег','Архангельск','+34134151');
insert into Customers values(13,'Вася','Борисов','+26234234324');
insert into Customers values(14,'Илья','Омск','+431413414');
insert into Customers values(15,'Коля','Пинск','+1312517761');

insert into Orders values(1,1,150);
insert into Orders values(2,1,500);
insert into Orders values(3,2,250);
insert into Orders values(4,2,300);
insert into Orders values(5,3,120);
insert into Orders values(6,1,1000);
insert into Orders values(7,2,60);
insert into Orders values(8,1,50);
insert into Orders values(9,1,500);
insert into Orders values(10,4,250);
insert into Orders values(11,2,25);
insert into Orders values(12,2,80);
insert into Orders values(13,4,320);
insert into Orders values(14,3,440);
insert into Orders values(15,4,120);
insert into Orders values(16,2,600);
insert into Orders values(17,4,200);
insert into Orders values(18,1,500);
insert into Orders values(19,4,800);
insert into Orders values(20,3,700);


-- ТОП 5 кастомеров по количеству заказов
-- ТОП 5 кастомеров по сумму платежей
-- Кастомеров с 5 и более заказов
-- Запросы для задания #3

select name_customer,count(*) as Count_Orders from Customers inner join Orders on Customers.id_customer=Orders.customer_id group by id_customer ORDER BY Count_Orders DESC limit 2;
 
select name_customer, sum(payment) as Sum_Orders from Customers inner join Orders on Customers.id_customer=Orders.customer_id group by id_customer ORDER BY Sum_Orders DESC limit 2;  

select name_customer,count(*) as Count_Orders from Customers inner join Orders on Customers.id_customer=Orders.customer_id group by id_customer HAVING Count(*) > 5;


 /*по набору order ids вывести информацию
 customers.name, customers.city, orders.payment
 отсортировать результат по городу, платежам
 вывести кастомеров у которых нету заказов
 вывести кастомеров у которых 10 и более заказов*/
 # Запросы для задания #4
 
select Customers.name_customer, Customers.city_customer, Orders.payment from Orders inner join Customers on Orders.customer_id=Customers.id_customer ORDER BY Customers.city_customer ASC, Orders.payment ASC;
 
select name_customer from Customers left join Orders on Customers.id_customer=Orders.customer_id where Orders.customer_id IS NULL;
 
select * from Customers;
select * from Orders;
