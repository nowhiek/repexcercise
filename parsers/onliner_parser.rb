require 'parsers/onliner_config'
require 'pry'

class OnlinerParser
  include OnlinerConfig

  def initialize
    p "======================="
    p CONFIG
    p "======================="
  end

  def run
    html = open(START_URL)
  	doc = Nokogiri::HTML(html)
 
    file_name = "./exports/onliner_#{Time.now.to_i}.csv"

    File.open(file_name,'a') do |file|
    	doc.xpath(CONFIG[:level_1][:xpath]).each do |node_1|
    		
    		data_id = node_1.xpath(CONFIG[:level_1][:data_id]).text
    		text_high_catalog = node_1.xpath(CONFIG[:level_1][:name]).text

    		doc.xpath(CONFIG[:level_2][:xpath].sub('%data_id%', data_id)).each do |node_2|
    			text_low_catalog = node_2.xpath(CONFIG[:level_2][:name]).text

          node_2.xpath(CONFIG[:level_3][:xpath]).each do |node_3|
            text_min_catalog = node_3.xpath(CONFIG[:level_3][:name]).text.strip
            text_href_low_catalog = node_3.xpath(CONFIG[:level_3][:href]).text
            file.write(text_high_catalog + " | " + text_low_catalog.strip + " | " + text_min_catalog + " | " + text_href_low_catalog + "\n")
          end
    		end	
    	end
    end

    puts "We create file - #{file_name}"
  end

end

OnlinerParser.new.run
