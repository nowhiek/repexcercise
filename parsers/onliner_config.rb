require 'open-uri'
require 'nokogiri'

module OnlinerConfig

  START_URL = 'https://catalog.onliner.by'
  
  # HIGH_LEVEL = doc.xpath()
  # length_high_level = HIGH_LEVEL.length
  # array_high_catalog_item = []
  # count = 0

  # while count!=length_high_level
  #   text_high_level_catalog = doc.xpath("//li[contains(@class, 'catalog-navigation-classifier__item')]")[count.to_i].xpath(".//span[contains(@class, 'catalog-navigation-classifier__item-title-wrapper')]").text
  #   array_high_catalog_item.push(text_high_level_catalog)
  #   count += 1
  # end


  CONFIG = {
    level_1: {
      xpath:"//li[contains(@class, 'catalog-navigation-classifier__item')]", #xpath
      name: ".//span[contains(@class, 'catalog-navigation-classifier__item-title-wrapper')]",
      data_id:"./@data-id"
    },
    level_2: {
      xpath: ".//div[contains(@class, 'catalog-navigation-list__category') and @data-id='%data_id%']//div[contains(@class, 'catalog-navigation-list__aside-item')]",
      name: "./div[contains(@class, 'catalog-navigation-list__aside-title')]"
    },
    level_3: {
      xpath: ".//div[contains(@class, 'catalog-navigation-list__dropdown-list')]//a[contains(@class, 'catalog-navigation-list__dropdown-item')]",
      name: ".//span[contains(@class, 'catalog-navigation-list__dropdown-title')]",
      href: "./@href"
    }


  }
end
