require 'pry'
require 'nokogiri'
require 'httparty'


class YandexParser

  def initialize
    @yandexuid = nil
  end

  def run
    #
    # Strategy:
    # open https://yandex.by/
    # extract cookie value for key yandexuid
    # extract "sk" value from page source
    # switch language with 'https://yandex.by/?lang=be&sk=<sk>'


    puts 'Sending initial request'
    start_page = open_url('https://yandex.by/', false)

    data_bem = JSON.parse(start_page.xpath('//body//div[contains(@class, "rows__row_last")]//div[contains(@data-bem, "portal/set/any")]/@data-bem').text)
    raise "Required JSON is not found" if data_bem.nil? || !data_bem.is_a?(Hash)


    url_with_sk = data_bem['media-grid']['collapseModSaveUrl'] # expected 'https://yandex.by/portal/set/any/?sk=ye934aaf0a9b43e153853a4d81f97a00d&gif=1&mfc=1'
    sk = url_with_sk[/sk=(\S+?)&/, 1]
    puts "SK value found: #{sk}"

    # bel
    url_to_change_lang = "https://yandex.by/?lang=be&sk=#{sk}"
    # kz
    # url_to_change_lang = "https://yandex.by/?lang=kz&sk=#{sk}"
    
    puts "Change language with url #{url_to_change_lang}"
    changed_page = open_url(url_to_change_lang, false)
    output_to_file(changed_page.to_html)
  end

  private

  def open_url(url, debug=false)
    domain = URI.parse(url).host.downcase

    headers = {}
    headers["User-Agent"] = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/68.0.3440.106",
    headers["Referer"] = @prev_domain if @prev_domain

    if @yandexuid
      headers["Cookie"] = "yandexuid=#{@yandexuid}"
    end

    response = HTTParty.get(url, {
      headers: headers,
      # debug_output: STDOUT, # To show that User-Agent is Httparty
    })
    
    
    # we need only one cookie value: yandexuid
    @yandexuid = response.headers['set-cookie'][/[\s^]yandexuid=(\d+)/, 1]

    doc = Nokogiri::HTML(response.body)
    binding.pry if debug

    doc
  end

  def output_to_file(str, path='./exports')
    File.open(File.join(path, 'debug.html'), 'wt') {|f| f.write(str)}
  end

end

YandexParser.new.run