class FileParser

  LARGE_FILE = 'exports/large_file'
  PO = 'exports/large_filee'
  SORTED_LARGE_FILE = 'exports/large_file_sorted'

  @@array = Array.new
  @@hash_values_and_keys = Hash.new

  def initialize
  end

  def run(action)
    puts "Start action #{action}"
    case action
    when 'gen_large_file'
      run_gen_large_file
    when 'read_large_file'
      run_read_large_file
    when 'sort_large_file'
      run_sort_large_file
    else
      raise "Unexpected action #{action}"
    end
  end

  private

  def run_gen_large_file
    count = 100000
    puts "Start writing file #{LARGE_FILE}"
    File.open(LARGE_FILE, 'wt') do |file|
      count.times do
        file.write((rand(100)+1).to_s + "\n")
      end
    end

    puts "File #{LARGE_FILE} was created with #{count} lines"
  end

  def run_read_large_file
    File.open(LARGE_FILE).each do |file|
      @@array << file.to_i
    end
  end
  

  def run_sort_large_file_hash
    
    @@array.each do |x|
      @@hash_values_and_keys[x] = @@array.count(x)
    end
    sorted_hash = Hash[@@hash_values_and_keys.sort_by{ |key, value| key}].to_h
    File.open(LARGE_FILE, 'wt') do |file|
      sorted_hash.each do |key, value|
        #puts (key.to_s + " : " + value.to_s + "\n")
        value.times do
          file.write(key.to_s + "\n")
        end
      end
    end
  end 

end

FileParser.new.run('gen_large_file')
FileParser.new.run('read_large_file')
FileParser.new.run('sort_large_file')
